﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DividerTrail : MonoBehaviour
{
    [SerializeField] private Transform _point1, _point2;
    [SerializeField] private AnimationCurve _curve;
    [SerializeField] private float _lerpRate;

    
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SmoothLerpPosition(_point1.position, _point2.position));
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private IEnumerator SmoothLerpPosition(Vector2 to, Vector2 from)
    {
        
        float counter = 0;
        while(counter < 1)
        {
            counter += Time.deltaTime / _lerpRate;
            transform.position = Vector2.Lerp(from, to, _curve.Evaluate(counter));
            yield return null;
        }
        StartCoroutine(SmoothLerpPosition(from, to));

        
    }

}
