﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jump : MonoBehaviour
{
    private Player2Movement _player2Movement;

    private void Awake()
    {
        _player2Movement = GetComponentInParent<Player2Movement>();
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            _player2Movement.Jump();

        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            _player2Movement.Jump();

        }
    }
}
