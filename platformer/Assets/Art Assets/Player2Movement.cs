﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Player2Movement : MonoBehaviour
{
    [SerializeField] private float _speedMultiplier;
    private BoxCollider2D _groundCheck;
    private float xInput;
    private Rigidbody2D _rigidBody;
    private float _jumpForce = 15;

    void Start()
    {
        _rigidBody = GetComponent<Rigidbody2D>();
        _groundCheck = GetComponent<BoxCollider2D>();

    }

    protected void Update()
    {
        GetPlayerInput();
        
    }

    private void GetPlayerInput()
    {
        xInput = Input.GetAxisRaw("Horizontal");
    }

    public void Jump()
    {
        _rigidBody.AddForce(new Vector2(0, 1) * _jumpForce, ForceMode2D.Impulse);
    }


    protected void FixedUpdate()
    {
        Move();
    }


    private void Move()
    {
        float yVelocity = _rigidBody.velocity.y;
       
        _rigidBody.velocity = new Vector2(xInput * _speedMultiplier, yVelocity);
    }



}