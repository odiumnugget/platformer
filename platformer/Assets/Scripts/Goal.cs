﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//***********************André*********************)
public class Goal : MonoBehaviour
{
    public ScoreManager _scoreManager;
    public Transform _resetPos1;
    public Transform _resetPos2;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<GoalTag>() != null && other.GetComponent<GoalTag>()._goal1Or2 == false)
        {
            _scoreManager.AddScoreP1();
            GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            transform.position = _resetPos1.position;
        }
        else if (other.GetComponent<GoalTag>() != null && other.GetComponent<GoalTag>()._goal1Or2 == true)
        {
            _scoreManager.AddScoreP2();
            GetComponent<Rigidbody2D>().velocity = new Vector2(0,0);
            transform.position = _resetPos2.position;
        }
    }
}
