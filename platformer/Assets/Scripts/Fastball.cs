﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//***************************André***********************)
public class Fastball : MonoBehaviour
{
    private Rigidbody2D _rigidbody;
    public float _fastballThreshold = 20;
    private bool _isFastball = false;
    public Transform _resetPos1;
    public Transform _resetPos2;
    public ScoreManager _scoreManager;
    private ParticleSystem _particles;
    private SpriteRenderer _sprite;

    private void Awake()
    {
        _rigidbody = GetComponentInParent<Rigidbody2D>();
        _particles = GetComponent<ParticleSystem>();
        _sprite = GetComponentInParent<SpriteRenderer>();
    }


    private void Update()
    {
        if (_rigidbody.velocity.magnitude > _fastballThreshold)
        {
            _isFastball = true;
            gameObject.GetComponentInParent<Renderer>().material.color = Color.red;
            _particles.Play();
            _sprite.material.color = Color.red;
            

        }
        else
        {
            _isFastball = false;
            gameObject.GetComponentInParent<Renderer>().material.color = Color.blue;
            _particles.Stop();
            _sprite.material.color = Color.white;
        }
    }

    private void OnTriggerEnter2D(Collider2D Player)
    {
        if (_isFastball == true)
        {
            if (Player.GetComponent<PlayerFastballTag>() != null)
            {
                if (Player.GetComponent<PlayerFastballTag>().Player1or2 == false)
                {
                    _scoreManager.AddScoreP1();
                    GetComponentInParent<Rigidbody2D>().velocity = new Vector2(0, 0);
                    GetComponentInParent<Rigidbody2D>().transform.position = _resetPos2.position;
                }
                else if (Player.GetComponent<PlayerFastballTag>().Player1or2 == true)
                {
                    _scoreManager.AddScoreP2();
                    GetComponentInParent<Rigidbody2D>().velocity = new Vector2(0, 0);
                    GetComponentInParent<Rigidbody2D>().transform.position = _resetPos2.position;
                }
            }
        }
         
    }
}
