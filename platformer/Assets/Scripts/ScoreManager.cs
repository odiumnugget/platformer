﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//***********************André************)
public class ScoreManager : MonoBehaviour
{
    public Text _p1ScoreUI, _p2ScoreUI;
    private int _player1Score = 0, _player2Score = 0;
    public GameObject _GoalSign;
    public Transform _PlayerResetP1, _PlayerResetP2, _Player1, _Player2;
    

    public void AddScoreP1()
    {
        _player1Score++;
        _p1ScoreUI.text = _player1Score.ToString();
        StartCoroutine(GOAL());
    }

    public void AddScoreP2()
    {
        _player2Score++;
        _p2ScoreUI.text = _player2Score.ToString();
        StartCoroutine(GOAL());
    }

    private IEnumerator GOAL()
    {
        GetComponent<AudioSource>().Play();
        ResetPlayers();
        _GoalSign.SetActive(true);
        yield return new WaitForSecondsRealtime(2);
        _GoalSign.SetActive(false);
    }

    public void ResetPlayers()
    {
        _Player1.position = _PlayerResetP1.position;
        _Player2.position = _PlayerResetP2.position;
    }
}
