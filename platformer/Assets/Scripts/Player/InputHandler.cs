﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** <author> Odysseas Ragnarson </author> **/

enum Players
{
    P1,
    P2
}

public class InputHandler : MonoBehaviour
{
    [SerializeField] Players _player;

    private string Horizontal, Vertical, Jump, Parry;

    private void Awake()
    {
        switch (_player)
        {
            case Players.P1:
                Horizontal = "Horizontal";
                Vertical = "Vertical";
                Jump = "Jump";
                Parry = "Parry";
                break;
            case Players.P2:
                Horizontal = "Horizontal2";
                Vertical = "Vertical2";
                Jump = "Jump2";
                Parry = "Parry2";
                break;
            default:
                break;
        }
    }
    public Vector2 GetAxialInput()
    {
        float xInput = Input.GetAxisRaw(Horizontal);
        float yInput = Input.GetAxisRaw(Vertical);
        return new Vector2(xInput, yInput);
    }

    public bool JumpKeyDown()
    {
        return Input.GetButtonDown(Jump);
    }

    public bool ParryKeyDown()
    {
        return Input.GetButtonDown(Parry);
    }


}
