﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** <author> Odysseas Ragnarson </author> **/

[RequireComponent(typeof(BoxCollider2D), typeof(InputHandler), typeof(MovementHandler))]
public class PlayerManager : MonoBehaviour
{
    private Vector2 _direction;
    private float xScale;
    private float yScale;

    #region Bookkeeping Variables

    private bool _jumping;
    private bool _grounded;
    private bool _dashUsed, _dashing;
    [SerializeField] private Transform _ballTransform;

    #endregion

    #region GroundedRay Variables 
    [Header("Ground Detection")]
    [SerializeField] private float _softGroundAllowance;
    [SerializeField] private LayerMask _ground;
    private int _numberOfrays = 4;
    private float verticalRayOffset = 0.025f;
    #endregion

    #region  Internal Components
    
    private BoxCollider2D _playerCollider;
    private InputHandler _inputHandler;
    private MovementHandler _movementHandler;
    private Parry _parry;
    private ParticleManager _particleManager;


    #endregion

    protected void Awake()
    {
        Initialize();
        xScale = transform.localScale.x;
        yScale = transform.localScale.y;
    }

    protected void Start()
    {
        InitializeListeners();
    }

    protected void OnDestroy()
    {
        DestroyListeners();
    }

    private void InitializeListeners()
    {
        OnDashResetCallback += DashResetCallback;
        _parry.OnBallParried += OnParrySuccessEvent;
        _parry.OnParryActivated += OnParryAttemptEvent;
    }

    

    private void DestroyListeners()
    {
        OnDashResetCallback -= DashResetCallback;
        _parry.OnBallParried -= OnParrySuccessEvent;
        _parry.OnParryActivated -= OnParryAttemptEvent;
    }

    private void Initialize()
    {
        
        _playerCollider = GetComponent<BoxCollider2D>();
        _inputHandler = GetComponent<InputHandler>();
        _movementHandler = GetComponent<MovementHandler>();
        _parry = GetComponentInChildren<Parry>();
        _particleManager = GetComponent<ParticleManager>();
        
    }

    protected void FixedUpdate()
    {        
        if (!_dashing)
        {
            _movementHandler.Move(_grounded, _direction.x);
        }
    }

    protected void Update()
    {
        GetStateVariables();
        HandleJumpAndDash();
        HandlePlayerFacing();
        #region Animiation
        //anim.SetBool("Jumping", _grounded);
        //anim.SetFloat("Speed", Mathf.Abs(_direction.x));
        #endregion
    }

    private void OnParryAttemptEvent()
    {
        _particleManager.InstantiateParryAtemptedParticle();
    }

    private void OnParrySuccessEvent()
    {
        _particleManager.InstantiateParrySucessfullParticle(_ballTransform.position);
    }

    private void HandleJumpAndDash()
    {
        if (_jumping && _grounded)
        {
            _movementHandler.Jump();
        }
        if (_jumping && !_grounded && !_dashUsed)
        {
            _dashUsed = true;
            _dashing = true;
            StartCoroutine(_movementHandler.Dash(_direction, OnDashResetCallback));
        }
        if(_grounded && _dashUsed)
        {
            _dashUsed = false;
        }
    }

    private Action OnDashResetCallback;

    private void DashResetCallback()
    {        
        _dashing = false;
    }
    
    private void GetStateVariables()
    {
        _direction = _inputHandler.GetAxialInput();
        _jumping = _inputHandler.JumpKeyDown();
        _grounded = IsGrounded();
    }

    private void HandlePlayerFacing()
    {        
        xScale = (_direction.x < 0) ? -xScale : xScale;
        transform.localScale = new Vector3(xScale, yScale, 1);
    }

    private bool IsGrounded()
    {
        bool grounded = false;
        Bounds bounds = _playerCollider.bounds;
        bounds.Expand(verticalRayOffset * -2);
        
        _numberOfrays = Mathf.Clamp(_numberOfrays, 2, int.MaxValue);
        float raySpacing = bounds.size.x / (_numberOfrays - 1);

        for (int i = 0; i < _numberOfrays; i++)
        {
            float rayLength = Mathf.Abs( _softGroundAllowance + verticalRayOffset);
            Vector2 rayOrigin = new Vector2(bounds.min.x, bounds.min.y);

            rayOrigin += Vector2.right * (raySpacing * i);
            RaycastHit2D hit = Physics2D.Raycast(rayOrigin, -Vector2.up, rayLength, _ground);
            Debug.DrawRay(rayOrigin, -Vector2.up * rayLength, Color.black);
            if (hit)
            {
                rayLength = hit.distance + _softGroundAllowance + 0.025f;
                grounded = true;                
            }            
        }
        return grounded;
    }

}
