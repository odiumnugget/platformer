﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//*******************André******************)
public class Parry : MonoBehaviour
{
    [SerializeField] private bool isPlayer1;

    private bool _isParrying = false;
    private CircleCollider2D _collider;
    private Vector2 _facing;
    public float _launchForce;
    private bool _isParryAble = false;
    private Collider2D _otherCollider;
    private float _inVelocity,_parryCooldown=0,_forceMultiplier = 1.1f;
    private InputHandler _inputHandler;


    private enum ParryStates
    {
        COLLIDED,
        RESET,
        NEWVECTOR2,
        REST

    }

    private ParryStates _currentState = ParryStates.COLLIDED;
    [SerializeField] private float _targetScaleMultiplier = 2.5f;

    private void Awake()
    {
        _collider = GetComponent<CircleCollider2D>();
        _inputHandler = GetComponentInParent<InputHandler>();        
    }

    private void Update()
    {
        if (_inputHandler.ParryKeyDown() && _isParrying == false)
        {
            StartCoroutine(DoParry());
            if(OnParryActivated != null)
            {
                OnParryActivated();
            }
        }

        

        Direction();
    }

    private void Direction()
    {        
        _facing = _inputHandler.GetAxialInput();

        if (_facing == Vector2.zero)
        {
            _facing = new Vector2(1, 1);
        }

    }


    private void OnTriggerEnter2D(Collider2D other)
    {


        
        if (other.GetComponent<Parryable>() !=null)
        {
            _otherCollider = other;

            if (_isParrying == true)
            {
                _isParryAble = true;
                StartCoroutine(ParryCycle());

            }
            else
            {
                    
            }
        }
    }
    public delegate void ParryEventHandler();
    public event ParryEventHandler OnBallParried;
    public event ParryEventHandler OnParryActivated;

    private IEnumerator ParryCycle()
    {
        if (_isParryAble == true)
        {
            _isParryAble = false;
            while (_isParryAble == false)
            {
                switch (_currentState)
                {
                    case ParryStates.COLLIDED:
                        _inVelocity = _otherCollider.attachedRigidbody.velocity.magnitude;
                        _currentState = ParryStates.RESET;
                        yield return 0.1;
                        break;
                    case ParryStates.RESET:
                        _otherCollider.attachedRigidbody.isKinematic = true;
                        _currentState = ParryStates.NEWVECTOR2;
                        if (OnBallParried !=null)
                        {
                            OnBallParried();
                        }
                        yield return 0.1;
                        break;
                    case ParryStates.NEWVECTOR2:
                        _otherCollider.attachedRigidbody.isKinematic = false;
                        _otherCollider.attachedRigidbody.velocity = _facing * Mathf.Max( _launchForce,(_inVelocity * _forceMultiplier));
                        _otherCollider.attachedRigidbody.angularVelocity = 0;
                        _currentState = ParryStates.REST;
                        yield return 0.1;
                        break;
                    case ParryStates.REST:
                        _currentState = ParryStates.COLLIDED;
                        _isParryAble = true;
                        yield return 0.1;
                        break;
                }
            }
        }

    }

    private IEnumerator DoParry()
    {
        _isParrying = true;
        Vector2 originalScale = transform.localScale;
        Vector2 targetScale = Vector2.one * _targetScaleMultiplier;
        float counter = 0;
        float lerprate = 0.1f;

        while (counter < 1)
        {
            counter += Time.deltaTime / lerprate;
            transform.localScale = Vector2.Lerp(originalScale, targetScale, counter);
            yield return null;
        }


        transform.localScale = originalScale;

        while (_parryCooldown < 0.3)
        {
            _parryCooldown += Time.deltaTime;
            yield return 0;
        }
        _parryCooldown = 0;
        _isParrying = false;
        
    }


}
