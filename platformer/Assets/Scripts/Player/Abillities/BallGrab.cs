﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** <author> Odysseas Ragnarson </author> **/

public class BallGrab : MonoBehaviour
{
    [SerializeField] private Rigidbody2D _playerRigidbody;
    [SerializeField] private Transform _grabPoint;

    private Rigidbody2D _grabbedObject;

    private bool _grabKeyDown;
    private bool _grabbing;
    
    void Update()
    {
        _grabKeyDown = Input.GetKeyDown(KeyCode.Keypad8);        
    }

    private void FixedUpdate()
    {
        Throw();
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        Grab(other);
    }

    private void Throw()
    {
        if (_grabbing && _grabKeyDown)
        {
            if (_grabbedObject)
            {
                _grabbedObject.GetComponent<IGrabbable>().Release();
                AddThrowForce(_grabbedObject);
                _grabbedObject = null;
            }
            StartCoroutine(TriggerNotActive());
            _grabbing = false;
        }
    }

    private void AddThrowForce(Rigidbody2D grabbedObject)
    {
        grabbedObject.AddForce(Vector2.right * 25f, ForceMode2D.Impulse);
    }

    private void Grab(Collider2D other)
    {
        if (other.GetComponent<IGrabbable>() != null && !_grabbedObject)
        {
            if (_grabKeyDown && !_grabbing)
            {
                other.GetComponent<IGrabbable>().Attach(_grabPoint);
                _grabbedObject = other.GetComponent<Rigidbody2D>();
                _grabbing = true;
            }
        }
    }

    private IEnumerator TriggerNotActive()
    {
        gameObject.GetComponent<Collider2D>().enabled = false;
        yield return new WaitForSeconds(0.125f);
        gameObject.GetComponent<Collider2D>().enabled = true;
    }
}
