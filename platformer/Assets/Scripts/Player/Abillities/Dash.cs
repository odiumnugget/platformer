﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** <author> Odysseas Ragnarson </author> **/

public class Dash : MonoBehaviour
{
    [SerializeField] private float _dashSpeed;

    public float _dashCooldown, _dashTime;
    
    public bool _dashing { get; private set; }

    

    private Rigidbody2D _rb;


    private void Awake()
    {
        _rb = GetComponent<Rigidbody2D>();
    }

    public void DoDash(Vector2 direction)
    {
        _rb.AddForce(direction * _dashSpeed, ForceMode2D.Impulse);
    }

    public void InvokeDashTime()
    {
        Invoke("Reset", _dashTime);
    }

    private void Reset()
    {
        _dashing = false;
    }
}
