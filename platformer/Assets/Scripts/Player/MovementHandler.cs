﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** <author> Odysseas Ragnarson </author> **/

[RequireComponent(typeof(Rigidbody2D))]
public class MovementHandler : MonoBehaviour
{
    [Space]
    [Header("Movement & Jump")]
    [Space]
    [SerializeField] private float _moveSpeedMultiplier = 10f;
    [SerializeField] private float _jumpForce = 18f;
    [SerializeField] private float _airborneMoveSpeedMultiplier = 6f;
    [Space]
    [Header("Dashing")]
    [Space]
    [SerializeField] private float _dashSpeed = 35f;
    [SerializeField] private float _dashTime = 0.35f;

    public float _yVelocity { get; private set; }

    private Rigidbody2D _playerRigidbody;
    private Dash _dash;

    private bool _jumpUsed;
    private float _originalGravity;

    protected void Awake()
    {
        _playerRigidbody = GetComponent<Rigidbody2D>();
        _dash = GetComponent<Dash>();
    }

    public void Move(bool grounded, float horizontalInput)
    {        
        float speed = grounded ? _moveSpeedMultiplier : _airborneMoveSpeedMultiplier;        
        _playerRigidbody.velocity = new Vector2(speed * horizontalInput, _playerRigidbody.velocity.y);        
    } 

    public void Jump()
    {
        _playerRigidbody.AddForce(new Vector2(0f, _jumpForce), ForceMode2D.Impulse);        
    }    

    protected void Start()
    {
        _originalGravity = _playerRigidbody.gravityScale;        
    }

    public IEnumerator Dash(Vector2 direction, Action callback)
    {
        float counter = 0;        
        if(direction == Vector2.zero)
        {
            direction = Vector2.right;
        }
        while(counter < _dashTime)
        {
            counter += Time.deltaTime;
            _playerRigidbody.gravityScale = 0;
            _playerRigidbody.velocity = direction.normalized * _dashSpeed;
            yield return null;
        }
        _playerRigidbody.gravityScale = _originalGravity;
        callback();
    }
}
