﻿using UnityEngine;
internal interface IGrabbable
{
    void Attach(Transform grabPoint);
    void Release();
}