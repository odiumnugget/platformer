﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** <author> Odysseas Ragnarson </author> **/

public class BallBehaviour : MonoBehaviour, IGrabbable
{
    private Rigidbody2D _rigidbody;
    private bool _grabbed;

    void Start()
    {
        _rigidbody = GetComponent<Rigidbody2D>();    
    }   

    public void Attach(Transform grabPoint)
    {
        _rigidbody.isKinematic = true;
        _rigidbody.velocity = Vector2.zero;
        _rigidbody.angularVelocity = 0;
        _rigidbody.transform.SetParent(grabPoint);        
        StartCoroutine(SmoothPickup(grabPoint.position));

    }

    public void Release()
    {
        _rigidbody.isKinematic = false;
        _rigidbody.transform.SetParent(null);
    }    
    
    private IEnumerator SmoothPickup(Vector2 grabPoint)
    {
        float counter = 0f;
        float smoothRate = 0.065f;
        Vector2 originalPosition = transform.position;
        while(counter < 1)
        {
            counter += Time.deltaTime / smoothRate;
            transform.position = Vector2.Lerp(originalPosition, grabPoint, counter);
            yield return null;
        }
    }

}
