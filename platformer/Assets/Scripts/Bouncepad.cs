﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//*********************André*************)
public class Bouncepad : MonoBehaviour
{
    private float _jumpForce = 30;

    private void OnTriggerStay2D(Collider2D player)
    {
        
        if (player.GetComponent<BouncepadPlayer>() !=null)
        {
            player.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 1) * _jumpForce, ForceMode2D.Impulse);
            GetComponent<AudioSource>().Play();
        }
    }
}
