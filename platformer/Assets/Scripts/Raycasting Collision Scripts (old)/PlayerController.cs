﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent (typeof (PlayerMovementEngine))]
public class PlayerController : MonoBehaviour
{
    [SerializeField] private float _jumpHeight = 4;
    [SerializeField] private float _timetToJumpApex = .4f;
    [SerializeField] private float _moveSpeed = 6;
    [SerializeField] private float _timeToSoftGround = .125f;

    private bool _grounded;
    private bool _hasBegunSoftGround;
    private float _jumpVelocity;
    private float _gravity;

    private Vector3 _velocity;
    private PlayerMovementEngine _controller;

    private void Start()
    {
        _controller = GetComponent<PlayerMovementEngine>();
        _hasBegunSoftGround = false;
        _gravity = -(2 * _jumpHeight) / Mathf.Pow(_timetToJumpApex, 2);
        _jumpVelocity = Mathf.Abs(_gravity) * _timetToJumpApex;
    }

    private void Update()
    {
        if(_controller._collisions.above || _controller._collisions.bellow)
        {
            _velocity.y = 0;
        }

        Vector2 input = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

        if (_controller._collisions.bellow && !_hasBegunSoftGround)
        {
            StartCoroutine(SoftGrounded());
        }

        if (Input.GetKeyDown(KeyCode.Space) && _grounded)
        {
            _velocity.y = _jumpVelocity;
        }        
        _velocity.x = input.x * _moveSpeed;
        _velocity.y += _gravity * Time.deltaTime;
        _controller.Move(_velocity * Time.deltaTime);
       
    }

    private IEnumerator SoftGrounded()
    {
        _hasBegunSoftGround = true;
        float timer = 0f;        
        while(timer < _timeToSoftGround)
        {            
            _grounded = true;
            timer += Time.deltaTime;
            yield return null;
        }
        _grounded = false;
        _hasBegunSoftGround = false;
    }
}
