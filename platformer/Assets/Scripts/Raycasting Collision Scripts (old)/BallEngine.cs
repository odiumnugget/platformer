﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(CircleCollider2D))]
public class BallEngine : MonoBehaviour
{
    [SerializeField] private LayerMask _collisionMask; 


    private CircleCollider2D _collider;
    private int _numberOfRays = 8;
    private float _raySpacing;

    private Vector2 _velocity;
    private float _radius;
    private float _gravity = -1f;
    private List<Vector2> _raycastPoints;


    private void Start()
    {
        _raycastPoints = new List<Vector2>();
        _collider = GetComponent<CircleCollider2D>();
        _radius = _collider.radius;
        _raySpacing = 2 * Mathf.PI  / _numberOfRays;
        
    }

    

    private void Update()
    {
        //CalculateRayOrigins();
        CalculateRayOrigins(ref _velocity);

        //Collisions(ref _velocity);
        transform.Translate(_velocity);
        _velocity.y += _gravity * Time.deltaTime;
        //Debug.Log(_velocity);
        //UpdateRayPositions();
    }

    //private void Collisions(ref Vector2 velocity)
    //{
    //    RaycastHit2D hit = Physics2D.CircleCast(transform.position, _radius, Vector2.zero, 0f, _collisionMask);
    //    //Debug.Log(hit.collider.gameObject);
    //    if (hit)
    //    {
    //        velocity.y = 0;
    //    }
    //}

    private void UpdateRayPositions()
    {
        for (int i = 0; i < _raycastPoints.Count; i++)
        {
            Vector2 dir = -(_raycastPoints[i] - (Vector2)transform.position);

            RaycastHit2D hit = Physics2D.Raycast(_raycastPoints[i], dir, 1f, _collisionMask);
            Debug.DrawRay(_raycastPoints[i], dir, Color.red);
        }
    }

    private void CalculateRayOrigins(ref Vector2 velocity)
    {
        _raycastPoints.Clear();
        float radius = _collider.radius;
        //Debug.Log(radius);
        float angle = 0;
        Vector2 center = transform.position;
        for (int i = 0; i < _numberOfRays; i++)
        {
            float x = Mathf.Sin(angle);
            float y = Mathf.Cos(angle);
            angle += 2 * Mathf.PI / _numberOfRays;
            
            Vector2 point = new Vector2(x, y);
            RaycastHit2D hit = Physics2D.Raycast(center, point, 10f, _collisionMask);
            if (hit && hit.distance <= radius)
            {
                velocity.y = 0;
            }
            //_raycastPoints.Add(point);
            Debug.DrawRay(center, point * 25, Color.red);
        }
    }

    private struct CollisionPoints
    {
        bool topLeft, top, topRight;
        bool left, right;
        bool bottomLeft, bottom, bottomRight;

        public void Reset()
        {
            topLeft = top = topRight = false;
            left = right = false;
            bottomLeft = bottom = bottomRight = false;
        }
    }

    private struct RaycastOrigins
    {
        Vector2 topLeft, top, topRight;
        Vector2 left, right;
        Vector2 bottomLeft, bottom, bottomRight;
    }


}
