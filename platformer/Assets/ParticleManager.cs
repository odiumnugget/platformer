﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleManager : MonoBehaviour
{
    [SerializeField] private GameObject _parrySucessfullParticle;
    [SerializeField] private GameObject _ParryAttemptParticle;

   
    private void Awake()
    {
        
    }

    public void InstantiateParrySucessfullParticle(Vector2 position)
    {
        
        GameObject particle = Instantiate(_parrySucessfullParticle, position, Quaternion.identity);
        StartCoroutine(Destroy(particle, 0.5f));
    }

    public void InstantiateParryAtemptedParticle()
    {
        
        GameObject particle = Instantiate(_ParryAttemptParticle, transform.position, Quaternion.identity, transform);
        StartCoroutine(Destroy(particle, 0.5f));
    }

    private IEnumerator Destroy(GameObject go, float time)
    {
        yield return new WaitForSeconds(time);
        Destroy(go);
    }
}
